require "HTTP.HTTPclient"
require "buildTasks"
require "gitCommands"

DIR_SEP = package.config:split("\n")[1]

function main(Data)
   -- Configure main branch name and source control commits url	
   local Config = component.fields()
   local BuildFolder = Config.BuildFolder
   local MainBranchName = Config.MainBranchName
   local RepoCommitsUrl = Config.RepoCommitsUrl
   local BuildResultsLocation = Config.BuildResultsLocation
   local BuildResultsLink = Config.BuildResultsLink .. os.whichOs() .. "/"
   local PushKey = Config.PushKey
   local PushDest = Config.PushDest

   -- Execute Git actions to get latest commit
   GITcheckoutMainBranch(BuildFolder, MainBranchName)
   GITpull(BuildFolder)
   GITcheckoutCommit(BuildFolder, Data)
   local LogMsg = GITcommitReadLog(BuildFolder, Data)
   LogMsg = LogMsg:split("\n")

   -- Output starting message
   local SlackUserDetails = getSlackDetails(LogMsg)
   local CommitDetails = {}
   CommitDetails.fullId = Data
   CommitDetails.shortId = string.sub(Data,0,9)
   CommitDetails.commitLink = "<" .. RepoCommitsUrl .. CommitDetails.fullId .. "/|" .. CommitDetails.shortId ..">"
   CommitDetails.commitMsg = LogMsg[1]

   local BuildStartMessage = createStartMessage(os.whichOs(), SlackUserDetails, CommitDetails)

   -- Queue Message to Slack Notifier Neuron
   queue.push{data=BuildStartMessage}
   if not iguana.isTest() then
      cleanUpOldResults(BuildResultsLocation, 604800) -- second argument is the lifespan of results, in seconds
      iguana.log("Going to build commit: " .. Data)
      buildSpecifiedCommit(BuildFolder, CommitDetails.shortId, BuildResultsLocation, Data)
   end

   -- Output build finished message
   local BuildError = countBuildError(BuildResultsLocation, CommitDetails.shortId)
   if not iguana.isTest() then
      pushSpecifiedCommit(BuildFolder, CommitDetails.shortId, BuildResultsLocation, Data, PushKey, PushDest)
   end
   local BuildEndMessage = createFinishedMessage(os.whichOs(), SlackUserDetails, CommitDetails, BuildError, BuildResultsLink)

   -- Queue Message to Slack Notifier Neuron
   queue.push{data=BuildEndMessage}
end

queue.onDequeued = main

function createStartMessage(OperatingSystem, SlackDetails, CommitDetails)
   local StartMsg = {}
   table.insert(StartMsg, os.date("%X", os.time() - 5*60*60)) -- Adjusting time to GMT-5
   table.insert(StartMsg, OperatingSystem)
   table.insert(StartMsg, CommitDetails.commitLink)
   table.insert(StartMsg, "by")
   table.insert(StartMsg, SlackDetails.name)
   table.insert(StartMsg, "- build started -")
   table.insert(StartMsg, CommitDetails.commitMsg)
   return table.concat(StartMsg, " ")
end

function createFinishedMessage(OperatingSystem, SlackDetails, CommitDetails, BuildErrors, BuildResultsLink)
   local FinishMsg = {}
   table.insert(FinishMsg, os.date("%X", os.time() - 5*60*60)) -- Adjusting time to GMT-5
   table.insert(FinishMsg, OperatingSystem)
   table.insert(FinishMsg, CommitDetails.commitLink)
   table.insert(FinishMsg, "by")
   if (BuildErrors == 0) then
      table.insert(FinishMsg, SlackDetails.name)
      table.insert(FinishMsg, "- build succeeded -")
   else
      table.insert(FinishMsg, SlackDetails.mention)
      table.insert(FinishMsg, "- " .. BuildErrors .. " FAILURES -")
   end
   table.insert(FinishMsg, CommitDetails.commitMsg)
   table.insert(FinishMsg, "see")
   table.insert(FinishMsg, "<" .. BuildResultsLink .. CommitDetails.shortId .. "/|results>")
   return table.concat(FinishMsg, " ")
end

function getSlackDetails(LogMsg)
   if LogMsg[3] == "" then
      return {name=LogMsg[2], mention=LogMsg[2]}
   end
   local AuthorEmail = LogMsg[3]
   local Id, Name
   if not iguana.isTest() then
      Id, Name = getIdNameByEmail(AuthorEmail)
   else
      Id = "ID"
      Name = "NAME"
   end
   local UserMention
   if Id == "" then
      UserMention = AuthorEmail
      Name = AuthorEmail
   else
      UserMention = "<@" .. Id ..">"
   end
   return {name=Name, mention=UserMention}
end

function getIdNameByEmail(email)
   local response, code, headers = HTTPget{
      url="https://slack.com/api/users.lookupByEmail",
      headers = {
         Authorization = "Bearer xoxb-4799145553-1170061522240-djMpGzeq3TLrpfRkSVy3jBty"
      },
      parameters = {
         email = email
      }
   }
   response = json.parse(response)
   if response.ok then
      return response["user"]["id"], response["user"]["real_name"]
   else
      return "", ""
   end
end
